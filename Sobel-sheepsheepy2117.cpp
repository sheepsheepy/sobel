#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <string>
#include <math.h>

using namespace cv;
using namespace std;

int Gx(Mat src, int x, int y)
{
	int result = abs(src.at<uchar>(x + 1, y - 1) + src.at<uchar>(x + 1, y) * 2 + src.at<uchar>(x + 1, y + 1)
		- src.at<uchar>(x - 1, y - 1) - src.at<uchar>(x - 1, y) * 2 - src.at<uchar>(x - 1, y + 1));

	if (result != 0)
		return result;
	else
		return 0;
}

int Gy(Mat src, int x, int y)
{
	int result = abs(src.at<uchar>(x - 1, y + 1) + src.at<uchar>(x, y + 1) * 2 + src.at<uchar>(x + 1, y + 1)
		- src.at<uchar>(x - 1, y - 1) - src.at<uchar>(x, y - 1) * 2 - src.at<uchar>(x + 1, y - 1));

	if (result != 0)
		return result;
	else
		return 0;
}

int main(int argc, char *argv[])
{
	Mat imgS, imgD, imgPro;

	imgS = imread("input.jpg", CV_LOAD_IMAGE_GRAYSCALE); 

	Mat imgX(imgS.rows, imgS.cols, CV_8U, Scalar(0));
	Mat imgY(imgS.rows, imgS.cols, CV_8U, Scalar(0));


	
	imshow("Source image", imgS);

	for (int i = 1; i < imgS.rows - 1; i++)
	{
		for (int j = 1; j < imgS.cols - 1; j++)
		{
			imgX.at<uchar>(i, j) = Gx(imgS, i, j);
			imgY.at<uchar>(i, j) = Gy(imgS, i, j);
		}
	}

	imwrite("imgX.jpg ", imgX);
	imwrite("imgY.jpg ", imgY);

	addWeighted(imgX, 0.8, imgY, 0.8, 0, imgD);

	imwrite("output.jpg ", imgD);

	waitKey(0);

	return 0;
}