# C++ with OpenCV library function : Sobel

Before Using :

- Install OpenCV first.  
- The code runs on Visual Studio
- The library is OpenCV 3.44

About Sobel :

- [OpenCV Sobel function website](https://docs.opencv.org/2.4.13.7/doc/tutorials/imgproc/imgtrans/sobel_derivatives/sobel_derivatives.html) 
- [MonkeyCoding 阿洲的程式教學 找邊緣](http://monkeycoding.com/?tag=sobel)

Files:

- readme.md
- Sobel-sheepsheepy2117.cpp : write by myself
- Sobel.cpp : the one use OpenCV library
- License Copyright (c) 2019 sheepsheepy2117-OpenCV

## Description: Sobel-sheepsheepy2117.cpp 

This Youtube tutorial is helpful for me.=D  

[Finding the Edges (Sobel Operator) - Computerphile](https://www.youtube.com/watch?v=uihBwtPIBxM)  

![GxGY](GxGy.png "GxGy")  
![Line](line.png "Line")
![Not Line](non-line.png "Not Line")
 -------------

#### Gx()  
The direction of X.

``` c++
int Gx(Mat src, int x, int y)
{
	int result = abs(src.at<uchar>(x + 1, y - 1) + src.at<uchar>(x + 1, y) * 2 + src.at<uchar>(x + 1, y + 1)
		- src.at<uchar>(x - 1, y - 1) - src.at<uchar>(x - 1, y) * 2 - src.at<uchar>(x - 1, y + 1));

	if (result != 0)
		return result;
	else
		return 0;
}
```

#### Gy()
The direction of Y.

``` c++
int Gy(Mat src, int x, int y)
{
	int result = abs(src.at<uchar>(x - 1, y + 1) + src.at<uchar>(x, y + 1) * 2 + src.at<uchar>(x + 1, y + 1)
		- src.at<uchar>(x - 1, y - 1) - src.at<uchar>(x, y - 1) * 2 - src.at<uchar>(x + 1, y - 1));

	if (result != 0)
		return result;
	else
		return 0;
}
```
--------------
![input](input.jpg "Inputimage")  
![imgX](imgX.jpg "imgXway")
![imgY](imgY.jpg "imgYway")  
![output](output.jpg "Outputimage") 

![outputbyOpencv](imgD.jpg "outputbyOpencv") 

## LICENSE
Copyright (c) 2019 sheepsheepy2117-OpenCV